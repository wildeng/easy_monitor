# frozen_string_literal: true

require_dependency 'easy_monitor/application_controller'
require 'redis'

module EasyMonitor
  class CachingChecksController < ApplicationController
    protect_from_forgery

    def memcached_alive
      head :no_content if memcached_alive?
    rescue EasyMonitor::Util::Errors::MemcachedNotWorking
      log_message('Memcached is not available')
      head :service_unavailable
    rescue EasyMonitor::Util::Errors::MemcachedNotUsed
      log_message('Memcached is not set up')
      head :not_implemented
    end

    def redis_alive
      redis_alive_message if connect_to_redis
    rescue Redis::CannotConnectError
      log_message(
        "Redis server at #{EasyMonitor::Engine.redis_url} is not responding"
      )
      redis_not_setup_message
    rescue StandardError => e
      log_message(
        "An error occurred #{EasyMonitor::Engine.redis_url} #{e.message}"
      )
      redis_error_message
    end

    private

    def redis_alive_message
      render json: {
        message: 'Redis is alive'
      }, status: 200
    end

    def redis_not_setup_message
      render json: {
        message: 'Redis is not responding or not set up'
      }, status: 200
    end

    def redis_error_message
      render json: {
        message: 'There is something wrong with Redis'
      }, status: 200
    end

    def connect_to_redis
      EasyMonitor.redis_ping
    end

    def memcached_alive?
      raise EasyMonitor::Util::Errors::MemcachedNotUsed\
        unless EasyMonitor::Engine.use_memcached
      raise EasyMonitor::Util::Errors::MemcachedNotWorking\
        unless EasyMonitor::Engine.cache

      EasyMonitor::Util::Connectors::MemcachedConnector.new(
        EasyMonitor::Engine.cache
      ).memcached_alive?
    end
  end
end
